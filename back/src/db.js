import sqlite from 'sqlite';
import SQL from 'sql-template-strings';


  const initializeDatabase = async () => {
  const db = await sqlite.open('./db.sqlite');


  const getAllMusicians = async() => {
    let returnString = ""
    const rows = await db.all("SELECT * FROM Artifexes WHERE title = 'Musician'")
    rows.forEach( ({ artist_id, username, password, name, email, title, about, comment, category, image, url  }) => returnString+=`[id:${artist_id}] - ${username} - ${password} - ${name} - ${email} - ${title} - ${about} - ${comment} - ${category} - ${image} - ${url}` )
    return rows;
  }

  const getAllActors = async() => {
    let returnString = ""
    const rows = await db.all("SELECT * FROM Artifexes WHERE title = 'Actor' or 'Actress'")
    rows.forEach( ({ artist_id, username, password, name, email, title, about, comment, category, image, url  }) => returnString+=`[id:${artist_id}] - ${username} - ${password} - ${name} - ${email} - ${title} - ${about} - ${comment} - ${category} - ${image} - ${url}` )
    return rows;
  }
  
  const getAllWriters = async() => {
    let returnString = ""
    const rows = await db.all("SELECT * FROM Artifexes WHERE title = 'Writer'")
    rows.forEach( ({ artist_id, username, password, name, email, title, about, comment, category, image, url  }) => returnString+=`[id:${artist_id}] - ${username} - ${password} - ${name} - ${email} - ${title} - ${about} - ${comment} - ${category} - ${image} - ${url}` )
    return rows;
  }

  const getAllArtists = async() => {
    let returnString = ""
    const rows = await db.all("SELECT * FROM Artifexes WHERE title = 'Artist'")
    rows.forEach( ({ artist_id, username, password, name, email, title, about, comment, category, image, url }) => returnString+=`[id:${artist_id}] - ${username} - ${password} - ${name} - ${email} - ${title} - ${about} - ${comment} - ${category} - ${image} - ${url}` )
    return rows;
  }

  const getAllDancers = async() => {
    let returnString = ""
    const rows = await db.all("SELECT * FROM Artifexes WHERE title = 'Dancer'")
    rows.forEach( ({ artist_id, username, password, name, email, title, about, comment, category, image, url  }) => returnString+=`[id:${artist_id}] - ${username} - ${password} - ${name} - ${email} - ${title} - ${about} - ${comment} - ${category} - ${image} - ${url}` )
    return rows;
  }
  const getAllArtifexes = async () => {
    let returnString = ""
    const rows = await db.all("SELECT * FROM Artifexes")
    rows.forEach( ({ artist_id, username, password, name, email, title, about, comment, category, image, url  }) => returnString+=`[id:${artist_id}] - ${username} - ${password} - ${name} - ${email} - ${title} - ${about} - ${comment} - ${category} - ${image} - ${url}` )
    return rows
  }
  
  const addArtifexes = async (props) => {
    console.log(props)
   if(!props || !props.username || !props.password || !props.name || !props.email || !props.title || !props.about || !props.comment || !props.category || !props.image || !props.url ){
     throw new Error ('you must provide properties')
   }
    const { username, password, name, email, title, about, comment, category, image, url  } = props
    const result = await db.run(SQL`INSERT INTO Artifexes (username, password, name, email, title, about, comment, category, image, url) VALUES (${username}, ${password}, ${name}, ${email}, ${title}, ${about}, ${comment}, ${category}, ${image} , ${url})`);
    const artist_id = result.stmt.lastID
    return artist_id
  }

  const deleteArtifexes = async (artist_id) => {
    const result = await db.run(SQL`DELETE FROM Artifexes WHERE artist_id = ${artist_id}`);
    if(result.stmt.changes === 0){
      return false
    }
    return true
  }
  const updateArtifexes = async (id, props) => {
    const { username, password, name, email, title, about, comment, category, image, url  } = props
    console.log(image)
    const result = await db.run(SQL`UPDATE Artifexes SET username=${username}, password=${password}, name=${name}, email=${email}, title=${title}, about=${about}, comment=${comment}, category=${category}, image=${image}, url=${url}  WHERE artist_id = ${id}`);
    if(result.stmt.changes === 0){
      return false
    }
    return true
  }
  
  const getArtifexes = async (id) => {
    const AllArtifexes = await db.all(SQL`SELECT artist_id AS id, username, password, name, email, title, about, comment, category, image, url FROM Artifexes WHERE artist_id = ${id}`);
    const Artifexes = AllArtifexes[0]
    return Artifexes
  }
  


  const controller = {
    addArtifexes,
    deleteArtifexes,
    getArtifexes,
    updateArtifexes,
    getAllArtifexes,
    getAllDancers,
    getAllActors,
    getAllArtists,
    getAllMusicians,
    getAllWriters
  }

  return controller
}
  


export default initializeDatabase
