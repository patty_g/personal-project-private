import app from './app'
import initializeDatabase from './db'
import path from 'path'
import multer from 'multer'


const multerStorage = multer.diskStorage({
  destination: path.join(__dirname, '../public/images'),
  filename: (req, file, cb) => {
    const { fieldname, originalname } = file
    const date = Date.now()
    // filename will be: image-1345923023436343-filename.png
    const filename = `${fieldname}-${date}-${originalname}`
    cb(null, filename)
  }
})
const upload = multer({ storage: multerStorage })

const start = async () => {
  
  const controller = await initializeDatabase()

  app.get('/Artifexes/dancers/list', async (req, res) => {
    const list = await controller.getAllDancers()
    console.log(list)
    res.json({result: list})
  })


  app.get('/Artifexes/musicians/list', async (req, res) => {
    const list = await controller.getAllMusicians()
    console.log(list)
    res.json({result: list})
  })

  app.get('/Artifexes/actors/list', async (req, res) => {
    const list = await controller.getAllActors()
    console.log(list)
    res.json({result: list})
  })

  app.get('/Artifexes/writers/list', async (req, res) => {
    const list = await controller.getAllWriters()
    console.log(list)
    res.json({result: list})
  })

  app.get('/Artifexes/artists/list', async (req, res) => {
    const list = await controller.getAllArtists()
    console.log(list)
    res.json({result: list})
  })



  app.get('/Artifexes/list', async (req, res) => {
    const list  = await controller.getAllArtifexes()
    res.json({result: list})
   })
  

  app.post('/Artifexes/add',upload.single('image'), async (req, res,next) => {
    try{
      const { username, password, name, email, title, about, comment, category, url } = req.query
    const image = req.file && req.file.filename
    console.log(req.query)
    const result = await controller.addArtifexes({ username, password, name, email, title, about, comment, category, image, url })
    res.json({ success: true, result })
  }
    catch (e) {
      next(e);
    }
  })

   app.get('/Artifexes/delete/:artist_id', async (req, res, next) => {
    const { artist_id } = req.params
    const result = await controller.deleteArtifexes(artist_id)
    res.json({success:true, result})
  })
  
  app.post('/Artifexes/update/:id',upload.single('image'), async (req, res, next) => {
    console.log("here")
    const { id } = req.params
    const { username, password, name, email, title, about, comment, category, url } = req.query
    const image = req.file && req.file.filename
    console.log(image)
    console.log(req.query, "---", id)
    const result = await controller.updateArtifexes(id,{username, password, name, email, title, about, comment, category, image, url})
    res.json({success:true, result})
  })
  
  app.get('/Artifexes/get/:id', async (req, res, next) => {
    const { id } = req.params
    const Artifexes = await controller.getArtifexes(id)
    res.json({success:true, result:Artifexes})
  })
  
  app.use((err, req, res, next) => {
    console.error(err)
    const message = err.message
    res.status(500).json({ success: false, message })
  })




  app.listen(8027, () => console.log('server listening on port 8027'))
}
start();
