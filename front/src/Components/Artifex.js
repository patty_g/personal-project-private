import React, { Component } from 'react';

import '../styles/artifexes.css'

class Artifex extends Component {


  state= {
    editMode :false,
    onChange:this.save
  }
  toggleEditMode = () => {
    const editMode = !this.state.editMode;
    this.setState({ editMode });
  };

  
  renderViewMode() {
    const {id,username,password,name,email,title,about,comment,category,image,deleteArtifexes} = this.props
    return (
      <div className="artistsDiv">
       <img src={`//localhost:8027/images/${image}`}  />
        
        <div className="artiststext">
        
     <p>{username}</p>
          <h2 style={{}}><b>{name}</b></h2> <br/>
          <h3>{title}</h3>
          <b>{about}</b>  
          <p>{category}</p> 
      
          </div>
          <div>
          <button onClick={() => deleteArtifexes(id)} >Delete</button>
          <input
                type="button"
                className="button"
                value="Edit"
                onClick={this.toggleEditMode}/> 
             </div>
        </div>
         
    )
  }

  save = e => {
    const { id, updateArtifexes } = this.props;
    e.preventDefault();
    const form = e.target;
    const username = form.username.value;
    const password = form.password.value;
    const name = form.name.value;
    const email = form.email.value;
    const title = form.title.value;
    const about = form.about.value;
    const comment = form.comment.value;
    const category = form.category.value;
    const image = form.artist_image_input.files[0];
    console.log("i am here", image)
    const url = form.url.value;
    updateArtifexes(id, { username, password, name, email, title, about, comment, category, image, url });
    this.toggleEditMode();
  }


  renderEditMode = () => {
    const {username, password, name, email, title, about, comment, category, image, url} = this.props
    return(
       
    <form 
     onSubmit={this.save}
     onReset={this.toggleEditMode}
     className="addform">
      <label for="name"> Username </label>
      <input className="form-control"
        type="text"
        name="username"
        defaultValue={username}
      />
   
      <label for="password"> Password </label>
      <input className="form-control"
        type="text"
        name="password"
        defaultValue={password}
      />
       <label for="name"> Name </label>
      <input className="form-control"
        type="text"
        name="name"
        defaultValue={name}
      />
      <label for="email"> Email </label>
      <input className="form-control"
        type="text"
        name="email"
        defaultValue={email}
      />
      <label for="title"> Title </label>
      <input className="form-control"
        type="text"
        name="title"
        defaultValue={title}
      />
      <label for="about"> About </label>
      <input className="form-control"
        type="text"
        name="about"
        defaultValue={about}
      />
      <label for="comment"> Comment </label>
      <input className="form-comment"
        type="text"
        name="comment"
        defaultValue={comment}
      
      />

<label for="category"> Category </label>
      <input className="form-control"
        type="text"
        name="category"
        defaultValue={category}
      />
       <label for="image"> Image </label>
      <input className="form-input"
        type="text"
        name="image"
        defaultValue={image}
      />
       <label for="url"> Post-url </label>
      <input className="form-control"
        type="text"
        name="url"
        defaultValue={url}
      />

        <input type="file" name="artist_image_input"/>
      <input className="form-btn" type="Submit" value="Submit" />
      <input
              type="button"
              className="button"
              value="Cancel"
              onClick={this.toggleEditMode} />
    </form>
      
    )
  }
    
  

  render() {
    const {editMode} = this.state;
      if(editMode) {
        return this.renderEditMode()
      }else{
          return this.renderViewMode()
        }
     

  }
}


export default Artifex;
