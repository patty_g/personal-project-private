
import SwiftSlider from 'react-swift-slider'
import React, { Component } from 'react';
import '../styles/main.css'


const data =  [
  {'id':'1','src':'http://bolognapac.com/wp-content/uploads/2013/06/compressed.jpg'},
  {'id':'2','src':'https://cdn-images-1.medium.com/max/1200/1*V5P68ZZDmEo0LnvjDD4Aig.png'},
  {'id':'3','src':'https://www.themindfulword.org/wp-content/uploads/2017/05/Dream-interpretation-through-imagination-we-manifest-our-deepest-dreams.jpg'},
  {'id':'4','src':'https://i.ytimg.com/vi/_nwYYFV03dM/maxresdefault.jpg'},
  {'id':'5','src':'https://www.hubbardstreetdance.com/media/1019/hsd121017_0909.jpg?center=0.73424657534246573,0.71666666666666667&mode=crop&width=1140&height=600&rnd=131478056190000000'},
  {'id':'6','src':'https://patch.com/img/cdn/users/22860462/2015/07/T800x600/20150755ad91ef5fc4f.jpeg'}

];
class slider extends Component {
  render() {
    return (
      <div>
      <SwiftSlider data={data}  height={700} enableNextAndPrev={false} showDots={false}/>
      </div>

    );
  }
}

export default slider;