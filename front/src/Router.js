import React from 'react';
import {BrowserRouter,Switch,Route} from 'react-router-dom';
//import Judges from './Pages/Judges';
//import Profile from './Pages/Profile';
import Artists from './Pages/artists';
import Homepage from './Pages/Homepage';
import Actors from './Pages/actors';
import Musicians from './Pages/musicians';
import Dancers from './Pages/dancers.js';
import About from './Pages/about';
import Contact from './Pages/contact';
import Profile from './Pages/profile';
import Artifexes from './Pages/Artifexes';
import Registration from './Pages/registration';
import Slider from './Components/slider'

const Router = () => (
  <BrowserRouter>
    <Switch>
    <Route exact path="/slider" component={Slider}/>
      <Route exact path="/Homepage" component={Homepage}/>
      <Route exact path="/about" component={About}/>
      <Route exact path="/musicians" component={Musicians}/>
      <Route exact path="/dancers" component={Dancers}/>
      <Route exact path="/actors" component={Actors}/>
      <Route exact path="/Artifexes" component={Artifexes}/>
      <Route exact path="/artists" component={Artists}/>
      <Route exact path="/profile" component={Profile}/>  
      <Route exact path="/contact" component={Contact}/>
      <Route exact path="/registration" component={Registration}/>
    </Switch>
  </BrowserRouter>
)


export default Router;
