import React, { Component } from 'react';
import InstagramEmbed from 'react-instagram-embed';
import logo from '../images/logoo.png'
import '../styles/artifexes.css'
import '../styles/main.css'
import breakdancer from '../images/breakdancer.jpg'
import icon1 from '../images/icon1.svg'
import icon2 from '../images/icon2.svg'
import icon3 from '../images/icon3.svg'
import icon4 from '../images/icon4.svg'
import icon5 from '../images/icon5.svg'

class Dancers extends Component {
  state = {
    dancers_list: []
  }
  getAllDancers = async order => {
    
    const response = await fetch(
      `http://localhost:8027/Artifexes/dancers/list`
    );
    const answer = await response.json();
    if (answer) {
      const dancers_list = answer.result;
      this.setState({dancers_list}, () => console.log(this.state.dancers_list))
  }
};
componentDidMount() {
  this.getAllDancers();
}
  render() {
    return (
      
      <div>
      <title>Artifex</title>
      <meta name="viewport" content="width=device-width, initial-scale=1" />
      <meta content="text/html; charset=iso-8859-2" httpEquiv="Content-Type" />
      <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css" />
      <link rel="stylesheet" href="./main.css" />
      <header className="header_area">
        <div className="main_menu">
          <nav className="navbar navbar-expand-lg navbar-light">
            <div className="container">
              {/* Brand and toggle get grouped for better mobile display */}
              <a className="navbar-brand logo_h" href="/Homepage"><img src={logo} alt style={{width: '250px', marginRight: '-40px'}} /></a>
              <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                <span className="icon-bar" />
                <span className="icon-bar" />
                <span className="icon-bar" />
              </button>
              {/* Collect the nav links, forms, and other content for toggling */}
              <div className="collapse navbar-collapse offset" id="navbarSupportedContent">
                <ul className="nav navbar-nav menu_nav justify-content-center">
                  <li className="nav-item"><a className="nav-link" href="/homepage">HOME</a></li>
                  <li className="nav-item"><a className="nav-link" href="/About">About us</a></li>
                  <li className="nav-item submenu dropdown">
                    <a className="nav-link dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">CATEGORIES</a>
                    <ul className="dropdown-menu">
                      <li className="nav-item"><a className="nav-link dropdown-color2" href="/Artists">Artists</a>
                      </li><li className="nav-item active"><a className="nav-link dropdown-color2" href="/Dancers">Dancers</a>
                      </li><li className="nav-item"><a className="nav-link dropdown-color2" href="/Musicians">Musicians</a>
                      </li><li className="nav-item"><a className="nav-link dropdown-color2" href="/Actors">Actors</a>
                      </li></ul>
                  </li>
                  <li className="nav-item"><a className="nav-link" href="/Contact">Contact</a></li> 
                </ul>
              </div>
            </div>
          </nav>
        </div>
      </header>
      <br /><br/> <br/> <br/>
      <div>
        <div><h1 className="cattitle">𝓓𝓪𝓷𝓬𝓮𝓻𝓼</h1></div>
     {/* <div className="clearfix" />
      <div className="row">
        <div className="portfolio-items">
          <div className="col-sm-6 col-md-4 col-lg-4 residential">
            <div className="portfolio-item">
              <div className="hover-bg"> <a href={breakdancer} title="Project Title" data-lightbox-gallery="gallery1">
                  <div className="hover-text">
                    <h4>Actors</h4>
                  </div>
                  <img src={breakdancer} className="img-responsive" alt="Project Title" /> </a> </div>
                  
            </div>
            
            
          </div>
          
        </div>
        
        </div>
        
    </div>
    <div>
      <div className="clearfix" />
      <div className="row">
        <div className="portfolio-items">
          <div className="col-sm-6 col-md-4 col-lg-4 residential">
            <div className="portfolio-item">
              <div className="hover-bg"> <a href={breakdancer} title="Project Title" data-lightbox-gallery="gallery1">
                  <div className="hover-text">
                    <h4>Actors</h4>
                  </div>
                  <img src={breakdancer} className="img-responsive" alt="Project Title" /> </a> </div>
                  
            </div>
            
            
          </div>
          
        </div>
        
        </div>
        
    </div>   <div>
      <div className="clearfix" />
      <div className="row">
        <div className="portfolio-items">
          <div className="col-sm-6 col-md-4 col-lg-4 residential">
            <div className="portfolio-item">
              <div className="hover-bg"> <a href={breakdancer} title="Project Title" data-lightbox-gallery="gallery1">
                  <div className="hover-text">
                    <h4>Actors</h4>
                  </div>
                  <img src={breakdancer} className="img-responsive" alt="Project Title" /> </a> </div>
                  
            </div>
           
            
          </div>
          
        </div>
        
        </div>*/}
        
    </div>

   <div className="dancers">
    {this.state.dancers_list.map((dancer) =>
      <div>
      <InstagramEmbed 
      url= {"https://www.instagram.com/" + dancer.url}
      maxWidth={340}
      maxHeight={50}
      hideCaption={true}
      containerTagName='div'
      protocol=''
      injectScript
      onLoading={() => {}}
      onSuccess={() => {}}
      onAfterRender={() => {}}
      onFailure={() => {}}
    />
    </div>    )} </div> 
    
<br/><br/><br/><br/><br/><br/><br/><br/><br/>
  <footer>
  <div className="container">
    <div className="row">
      <div className="col-md-5 col-md-offset-1 col-sm-6">
      <h3 style={{fontSize: '26px', marginBottom: '6px', fontFamily: '"Merriweather", serif', fontStyle: 'normal', fontWeight: 'bold', letterSpacing: '0.5px', marginTop: '20px',
marginBottom: '10px', color: '#FFEFBF'}}>Artifex</h3>
        <p className="foot">An online competition site which focuses on gathering different types of talents(artists,dancers, singers, poets,writers and actors) exposing them to the artist life through multiple rewards.
        The judges are selected through their years of experience in the art and how active they are in the art world.
          </p>
          <h2>Create Inspire Unite</h2>
        <div className="footer-copyright">
          <p className="foot">Copyright © 2019 Artifex </p>
        </div>
      </div>
      <div className="col-md-4 col-md-offset-1 col-sm-6">
        <h3 style={{fontSize: '26px', marginBottom: '6px', fontFamily: '"Merriweather", serif', fontStyle: 'normal', fontWeight: 'bold', letterSpacing: '0.5px', marginTop: '20px',
marginBottom: '10px', color: '#FFEFBF'}}>Talk to us</h3>
        <p className="foot" style={{lineHeight: '0.5'}}><i className="fa fa-globe" />Beirut, Lebanon</p>
        
        <p className="foot"><i className="fa fa-save" /> Artifex.World@gmail.com</p>
          <hr /><div className="col-md-12 col-sm-12">
       <div className="unk">
         <a href=""><img src={icon1}/> </a>
         <a href=""><img src={icon2}/> </a>
         <a href=""><img src={icon3}/> </a>
         <a href=""><img src={icon4}/> </a>
          <a href=""><img src={icon5}/></a>
  </div>
      </div>
        
      </div>
      <div className="clearfix col-md-12 col-sm-12">
       
      </div>
     
    </div>
  </div>
</footer>
      </div>
    );
  }
}

export default Dancers;
