import React, { Component } from 'react';
import '../App.css';
import '../styles/artifexes.css'
import Artifex from '../Components/Artifex';
class Artifexes extends Component {

  state = {
    artifexes_list: [],
    username:"",
    password:"",
    name:"",
    email:"",
    title:"",
    about:"",
    comment:"",
    category:"",
    image:"",
    url:"",
  };
  

  

  getArtifexes = async id => {
    // check if we already have the artist
    const previous_artist = this.state.artifexes_list.find(
      artifex => artifex.id === id
    );
    if (previous_artist) {
      return; // do nothing, no need to reload a artist we already have
    }
    try {
      const response = await fetch(`http://localhost:8027/Artifexes/get/${id}`);
      const answer = await response.json();
      if (answer.success) {
        // add the user to the current list of artists
        const artifex = answer.result;
        const artifexes_list = [...this.state.artifexes_list, artifex];
        this.setState({ artifexes_list });
      } else {
        this.setState({ error_message: answer.message });
      }
    } catch (err) {
      this.setState({ error_message: err.message });
    }
  };

  deleteArtifexes = async id => {
    try {
      const response = await fetch(
        `http://localhost:8027/Artifexes/delete/${id}`
      );
      const answer = await response.json();
      if (answer) {
        // remove the user from the current list of users
        const artifexes_list = this.state.artifexes_list.filter(
          artifex => artifex.id !== id
        );
        this.setState({ artifexes_list });
      } else {
        this.setState({ error_message: answer.message });
      }
    } catch (err) {
      this.setState({ error_message: err.message });
    }
  };

  updateArtifexes = async (id, { username,password,name,email,title,about,comment,category,image,url}) => {
    let body = null;
    console.log("heeeeerrreee")
    console.log("image", image)
    if (image){
      body = new FormData()
      body.append(`image`, image)
    }
  
    const response = await fetch(`http://localhost:8027/Artifexes/update/${id}?username=${username}&password=${password}&name=${name}&email=${email}&title=${title}&about=${about}&comment=${comment}&category=${category}&image=${image}&url=${url}`,
    {method: 'POST',
  body,
});
console.log(response)
    const answer = await response.json();
    console.log("answer of update: ", answer)
    if (answer) {
      const artifexes_list = this.state.artifexes_list.map(artifex => {
        if (artifex.artist_id === id) {

          const updatedArtifexes = {
            id: artifex.artist_id,
            username, 
            password, 
            name, 
            email, 
            title, 
            about, 
            comment, 
            category,
            image: artifex.image,
            url   }
          return updatedArtifexes
        }
      
      else {
         return artifex
        }
      });
      this.setState({ artifexes_list });
    }
  }


  addArtifexes = async (username,password,name,email,title,about,comment,category,image,url) => {
    let body = null;
    console.log("image", image)
    if(image){
      body = new FormData();
      body.append(`image`, image)
      console.log(body)
    }
    console.log(username, password, name, email, title, about, comment, category, image, url)
    const response = await fetch(`http://localhost:8027/Artifexes/add?username=${username}&password=${password}&name=${name}&email=${email}&title=${title}&about=${about}&comment=${comment}&category=${category}&image=${image}&url=${url}`,{
      method:'POST',
      body
    });
    const answer = await response.json();
    console.log(answer)
    if (answer) {
      const id = answer.result;
      const artifex = { username, password, name, email, title, about, comment, category, image, url, id }
      const artifexes_list = [...this.state.artifexes_list, artifex]
      this.setState({ artifexes_list })

    }
  }

  getAllArtifexes = async order => {
    
      const response = await fetch(
        `http://localhost:8027/Artifexes/list?order=${order}`
      );
      const answer = await response.json();
      if (answer) {
        const artifexes_list = answer.result;
        this.setState({artifexes_list}, () => console.log(this.state.artifexes_list))
    }
  };
  componentDidMount() {
    this.getAllArtifexes();
  }
  

  onSubmit = (e) => {
    
    // stop the form from submitting:
    e.preventDefault();
    // extract username, password, name, email, title, about, comment, category from state
    const image = e.target.fileField.files[0];
    console.log(this.state.title)
    // create the artist from username, password, name, email, title, about, comment, category
    this.addArtifexes(this.state.username,this.state.password,this.state.name,this.state.email,this.state.title,this.state.about,this.state.comment,this.state.category, image, this.state.url);
    // empty username, password, name, email, title, about, comment, category so the text input fields are reset
    this.setState({ username:"", password:"", name:"", email:"", title:"", about:"", comment:"", category:"", image:"", url:"" });
  };
  render() {
    const { artifexes_list } = this.state
    return (
     <div>
       <form onSubmit={this.onSubmit} className="addform">
      <div><h1>Artifex</h1></div>
       Username : 
          <input className="form-control"
          style={{width:"200px"}}
            type="text"
            onChange={e => this.setState({ username: e.target.value })}
            value={this.state.username}
             />

          <label for="password">Password : </label>
          <input className="form-control"
          style={{width:"200px"}}
            type="text"
            onChange={e => this.setState({ password: e.target.value })}
            value={this.state.password}
             />
             <div>
             <label for="name">Name : </label>
          <input className="form-control"
          style={{width:"200px"}}
            type="text"
            onChange={e => this.setState({ name: e.target.value })}
            value={this.state.name}
             />
             <label for="email">Email : </label>
             
          <input className="form-control"
            type="text"
            style={{width:"200px"}}
            onChange={e => this.setState({ email: e.target.value })}
            value={this.state.email}
             />
             </div>
<div>
<label for="title"> Title :</label>

<select  className="form-control" style={{width:"200px"}} onChange={e => this.setState({title: e.target.value })}>
            <option value="Artist">Artist</option>
            <option value="Dancer">Dancer</option>
            <option value="Actor">Actor/Actress</option>
            <option value="Musician">Musician</option>
          </select>
          
             

<label for="about">About : </label>
          <input className="form-control"
          style={{width:"200px"}}
            type="text"
            onChange={e => this.setState({ about: e.target.value })}
            value={this.state.about}
             /></div>
<div>
<label for="comment">Comment : </label>
          <input className="form-comment"
            type="text"
            style={{width:"200px"}}
            onChange={e => this.setState({ comment: e.target.value })}
            value={this.state.comment}
             />
                <label for="category">   Category : </label>
          <input className="form-control"
            type="text"
            style={{width:"200px"}}
            onChange={e => this.setState({ category: e.target.value })}
            value={this.state.category}
             /></div>
             <div>
               <label for="image">Image : </label>
            <input className="form-control"
            style={{width:"300px"}}  type="file" name="fileField"

            />

<label for="url">Url : </label>
          <input className="form-control"
          style={{width:"200px"}}
            type="text"
            onChange={e => this.setState({ url: e.target.value })}
            value={this.state.url}
             /></div>

       <input className="form-btn" type="Submit" value="Add" />
        </form>
        {artifexes_list.map(artifex => 
          <Artifex
            key={artifex.artist_id}
            id={artifex.artist_id}
            username={artifex.username}
            password={artifex.password}
            name={artifex.name}
            email={artifex.email}
            title={artifex.title}
            about={artifex.about}
            comment={artifex.comment}
            category={artifex.category}
            image={artifex.image}
            url={artifex.url}
            deleteArtifexes={this.deleteArtifexes}
            updateArtifexes={this.updateArtifexes}
          />
        )} 
       {/**  <div className="artistsDiv">
        {artists_list.map(artist =>
       <div> 
         <br/>
         <img src={artist1}/> 
       
        <p font-family="Merriweather Sans">
        <h1>{artist.name}</h1>
       <h2>{artist.title}</h2> 
        <p>{artist.about}</p>
        

        </p>
        {artist.category}
       </div>
        
        )}
        </div>
        */}
        
      </div>
    );
  }

}

export default Artifexes;
